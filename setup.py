from setuptools import setup, find_packages

setup(
    name='ci',
    version='1.0',
    author='Mario Marino',
    author_email='marinomario.17@gmail.com',
    packages=find_packages()
)